declare namespace App.Obj {

	export interface ErrorInterface {
		msg:string;
		type?:string;
		dt:number;
		id:number;
	}

	export interface NameStub {

		name:string;
		stub:string;

	}

	export interface NameUrl {
		name:string;
		url:string;
	}

	export interface AuthInterface {

		apiId:string;
		name:string;
		token:string;
		auth:number;

	}

	export interface ItemInterface {

		stub:string;
		data_likes:number;
		dt_created:number;
		user_liked?:number;
		text:string;
		img:string;
		data_comments:number;
		source:string;
		source_stub:string;
		link:string;

	}

	export interface queryParamsInterface {
		
		sortBy?:string;
		sourceType?:string;
		type?:string;
		source?:string;
	
	}

	export interface LocationObjectInterface {

		dt:number;
		lat:number;
		lon:number;

		country_code:string;
		country_lon:number;
		country_lat:number;
		country_name:string;

		country_continent:string;

	}

	export interface CommentInterface {

		stub:string;
		stub_parent:string;
		text:string;
		data_likes:number;
		data_comments:number;
		data_reports:number;
		dt:number;
		user_liked?:number;
		user_reported?:number;
		user_saved?:number;
		user:string|null;
		isDeleted:number;
		isRemoved:number
		user_owned:number|boolean;
	}

	export interface CommentQueryParamsInterface {
	
		item:string;
		parent?:string;
		stub?:string;
		upTo?:string;
		sortBy?:string;
	
	}

	export interface CommentLocationObjectInterface {
	
		text:string;
		item:string;
		comment:string;
		name:string;
		lat:number|null;
		lon:number|null;
		country_lon:number;
		country_lat:number;
		avatar:string|null;
	
	}

	export interface UserLocationInterface {

		latitude:number;
		longitude:number;
		accuracy:number;
		altitude:number;
		altitudeAccuracy:number;
		heading:number;
		speed:number;

	}

	export interface ItemLocationObjectInterface {

		id:number;
		lat:number;
		lon:number;
		stub:string;
		source_lat:number;
		source_lon:number;
		source_logo:string;

	}

	export interface SourceTypeSelector {

		rss:boolean;
		twitter:boolean;
		users:boolean;

	}

	export interface SourceInterface {
		data_likes:number;
		data_comments:number;
		data_false:number;
		stub:string;
		type:string;
	}

	export interface UserDataInterface {
		dt_register:number;
		profile:string;
		email:string|null;
		avatar:string|null;
		location:LocationObjectInterface;
		name:string;
	}

}