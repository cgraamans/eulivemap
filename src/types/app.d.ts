declare namespace App {

	export interface FormObjLoginRegisterData {

		name:string;
		password:string;

	}

	export interface FormObjLoginRegister {

		login: FormObjLoginRegisterData;
		reg: FormObjLoginRegisterData;
		persistent:boolean;

	}

	export interface FormObjValid {

		name:boolean;
		password:boolean;

	}

}