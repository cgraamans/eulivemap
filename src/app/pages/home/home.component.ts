import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';

import { DataService } from '../../services/data.service';
import { SocketService } from '../../services/socket.service';
import { MapService } from '../../services/map.service';
import { AppDataService } from '../../services/app-data.service';

import {Router, ActivatedRoute, Params, NavigationExtras } from '@angular/router';

import {Vector as VectorLayer,Tile as TileLayer } from 'ol/layer';
import {Vector as VectorSource, TileJSON } from 'ol/source';
import {transform, fromLonLat, toLonLat, METERS_PER_UNIT as OlMPU} from 'ol/proj';

import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';

import Collection from 'ol/Collection';
import { Modify } from 'ol/interaction';

import {Polygon,Circle,Point} from 'ol/geom';
import {Style, Stroke, Fill, Icon} from 'ol/style';
import {easeOut} from 'ol/easing.js';	

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalItemComponent } from '../../components/modal-item/modal-item.component';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

	private subscriptions = [];

	public isLoggedIn:boolean = false;
	public refreshTime = 3000;
	public scrollTo:string|null = null;

	public source:App.Obj.SourceInterface;

	private statics = null
	private map:Map;

	public modalRef: BsModalRef;

	private coords:Coordinates;

	public interfaceDisplay = {
		log:true,
	};

	public mapDisplay = {
		items:true,
		me:true,
		comments:true
	};

	constructor(
		private data:DataService,
		private socket:SocketService,
		private route:ActivatedRoute, 
		private router:Router,
		private location:Location,
		private mapSvc:MapService,
		private modalService:BsModalService,
		private appData:AppDataService, 
	) { 

		this.map = mapSvc.getMap();
		let that = this;

		this.map.on('click', function(evt) {

			let iCount = 0;
			let feature = that.map.forEachFeatureAtPixel(evt.pixel,function(feature, layer) {

				let item = feature.get('item');
				if(iCount <1 && item) { 

					that.scrollTo = item.stub;
					
					let coords = transform([item.source_lon, item.source_lat], 'EPSG:4326', 'EPSG:3857');
					if(item.lon && item.lat) coords = transform([item.lon, item.lat], 'EPSG:4326', 'EPSG:3857');

					let view = that.map.getView();
					view.animate({
						center:coords,
						duration:2000
					});

					that.interfaceToggle('log');

				}

				let commentObj = feature.get('comment');
				if(iCount <1 && commentObj) { 

					let navObj:NavigationExtras = {
						relativeTo: that.route,
						queryParams:{comment:commentObj.comment}
					};

					that.router.navigate(
						['item',commentObj.item], navObj
					);

				}

				iCount++;

			});

		});

		this.map.getCurrentScale = function () {

			let view = that.map.getView();
			if(view) {
				let resolution = view.getResolution();
				let units = view.getProjection().getUnits();		
				let mpu = OlMPU[units];
				let dpi = 25.4 / 0.28;

				if(resolution && units && mpu) return (resolution * mpu * 39.37 * dpi);

			}
			return false;

		};

		this.statics = data.getStaticObj();

	}

	ngOnInit() {
		
		this.subscriptions.push(this.socket.on('source').subscribe(obs=>{

			if(obs) {

				if(obs.ok && obs.results.length === 1) {
					
					this.source = obs.results[0];
					if(!this.interfaceDisplay['log']) this.interfaceToggle('log');

					let itemEmitParams:App.Obj.queryParamsInterface = this.appData.getItemEmitParameters();
					if(itemEmitParams) {
						itemEmitParams.source = this.source.stub;
						this.appData.setItemEmitParams(itemEmitParams);
					}

				} else {

					if(obs.msg) this.data.setError(obs.msg);

				}

			}

		}));

		this.subscriptions.push(this.data.getAuthObs().subscribe(obs=>{

			if(obs) {

				this.isLoggedIn = true;

			} else {

				this.isLoggedIn = false;
				this.newLayer('me');

			}

		}));

		// move to app.component
		this.subscriptions.push(this.data.getUserDataObs().subscribe(obs=>{
			
			let that = this;

			if(obs && this.map) {

				let currentLayer = this.newLayer('me');
				let uD = this.data.getUserData();

				if (this.isLoggedIn && currentLayer && uD) {

					let source = currentLayer.getSource();

					let coordArr = [obs.location.country_lon, obs.location.country_lat];
					if(obs.location.lat && obs.location.lon) coordArr = [obs.location.lon, obs.location.lat];

					let coords = transform(coordArr, 'EPSG:4326', 'EPSG:3857');
					let map = this.map;
					let meFeature = new Feature(new Point(coords));

					let meStyle;
					if(uD && uD.avatar) {

						meStyle = new Style({
							image: new Icon({
								src:this.statics.apiRoot + this.statics.avatars + uD.avatar +'.btn.png?ts='+(new Date()).getTime(),
								scale:0.66
							})
						});

					} else {

						meStyle = new Style({
							image: new Icon({
							// 	// anchor:coords,
								src:this.statics.apiRoot + this.statics.icons + 'eu.stars.png',
								scale:0.66
							})
						});

					}

					meFeature.on('change',function(){

						let lonLat = toLonLat(this.getGeometry().getCoordinates());
						if(lonLat){

							that.socket.emit('user.set.location',{lon:lonLat[0],lat:lonLat[1]});	

						}

					},meFeature);

					let modifyMe = new Modify({
						features: new Collection([meFeature])
    				});

    				meFeature.setStyle(meStyle);
					source.addFeature(meFeature);

					this.map.addInteraction(modifyMe);
					this.map.getView().setCenter(coords);

				}

				// this.socket.emit('app.user.location.set',obs);
			}

		}));

		this.subscriptions.push(this.appData.getCommentLocationsObs().subscribe(obs=>{

			let that = this;

			let currentLayer = this.getCurrentLayerByName('comments');
			if(obs && obs.length > 0 && this.map && currentLayer) {
			
				let source = currentLayer.getSource();
				let finalCoords = fromLonLat([-0.12755, 51.507222]);
				
				obs.forEach((commentLocation,idx)=>{

					let hasMe = false;
					source.getFeatures().forEach(function(feature) {
						let fId = feature.getId();
						if(fId && fId === commentLocation.comment) hasMe = true;
					});

					if(!hasMe) {

						let coords = transform([commentLocation.country_lon, commentLocation.country_lat], 'EPSG:4326', 'EPSG:3857');
						if(commentLocation.lon && commentLocation.lat) coords = transform([commentLocation.lon, commentLocation.lat], 'EPSG:4326', 'EPSG:3857');
						
						if(idx === 0) finalCoords = coords;

						let dup = false;
						source.getFeatures().forEach(function(feature) {
					   		if(feature.getGeometry().getCoordinates()[0] === coords[0] && feature.getGeometry().getCoordinates()[0] === coords[0]){
					   			dup = true;
					   		}
						});

						if(dup) {
							coords = [
								Math.random() < 0.5 ? coords[0] + (Math.random()*10000) : coords[0] - (Math.random()*10000),
								Math.random() < 0.5 ? coords[1] + (Math.random()*10000) : coords[1] - (Math.random()*10000)
							];
						}

						let src = this.statics.apiRoot + this.statics.icons + 'eu.stars.png';
						if(commentLocation.avatar) src = this.statics.apiRoot + this.statics.avatars + commentLocation.avatar + '.btn.png?ts='+(new Date()).getTime();
							
						let iconFeature = new Feature({
							geometry:new Point(coords),
							comment:commentLocation
						});

						let iconStyle = new Style({
							image: new Icon({
							// 	// anchor:coords,
								src:src,
								scale:0.66
							})
						});

						iconFeature.setId(commentLocation.comment);							
						iconFeature.setStyle(iconStyle);
						source.addFeature(iconFeature);

					}

				});
				this.interfaceDisplay.log = false;

			}

		}));

		// move to app.component
		this.subscriptions.push(this.appData.getItemLocationsObs().subscribe(obs=>{

			let that = this;
			let currentLayer = this.newLayer('items');
			let commentLayer = this.newLayer('comments');

			if(obs && obs.length > 0 && this.map) {

				let source = currentLayer.getSource();
				source.getFeatures().forEach(function(feature) {

					let fId = feature.getId();
					if(fId){
						let fLocIdx = obs.findIndex(x=>x.stub === fId);
						if(fLocIdx < 0) {
							source.removeFeature(feature);
						}
					}

				});
				let finalCoords = fromLonLat([-0.12755, 51.507222]);

				if(obs.length === 1) {
					that.socket.emit('comment.locations',{item:obs[0].stub});
				}
				obs.forEach((item,idx)=>{

					let hasMe = false;
					source.getFeatures().forEach(function(feature) {
						let fId = feature.getId();
						if(fId && fId === item.stub) hasMe = true;
					});

					if(!hasMe) {

						let coords = transform([item.source_lon, item.source_lat], 'EPSG:4326', 'EPSG:3857');
						if(item.lon && item.lat) coords = transform([item.lon, item.lat], 'EPSG:4326', 'EPSG:3857');
						
						if(idx === 0) finalCoords = coords;

						let dup = false;
						source.getFeatures().forEach(function(feature) {
					   		if(feature.getGeometry().getCoordinates()[0] === coords[0] && feature.getGeometry().getCoordinates()[0] === coords[0]){
					   			dup = true;
					   		}
						});

						if(dup) {
							coords = [
								Math.random() < 0.5 ? coords[0] + (Math.random()*10000) : coords[0] - (Math.random()*10000),
								Math.random() < 0.5 ? coords[1] + (Math.random()*10000) : coords[1] - (Math.random()*10000)
							];
						}

						let src = this.statics.apiRoot + this.statics.icons + 'eu.stars.png';
						if(item.source_logo) src = this.statics.apiRoot + this.statics.icons + item.source_logo;
							
						let iconFeature = new Feature({
							geometry:new Point(coords),
							item:item
						});

						let iconStyle = new Style({
							image: new Icon({
							// 	// anchor:coords,
								src:src,
								scale:0.66
							})
						});

						iconFeature.setId(item.stub);							
						iconFeature.setStyle(iconStyle);
						source.addFeature(iconFeature);

					}

				});
				// that.view.animate({
				// 	center: finalCoords,
				// 	duration: 2000
				// });

			}

		}));

	}

	ngOnDestroy() {

		this.subscriptions.forEach(subscription=>{

			subscription.unsubscribe();

		});

	}

	private getCurrentLayerByName(name){

		let currentLayer;
		this.map.getLayers().forEach(function (layer) {

			if(layer.get('name') && layer.get('name') === name) currentLayer = layer;

		});
		return currentLayer;

	}

	private newLayer(name:string){

		if(this.map) {

			let currentLayer = this.getCurrentLayerByName(name);
			if(currentLayer) this.map.removeLayer(currentLayer);

			let newLayer = new VectorLayer({
				source: new VectorSource({features:[]}),
			});
			newLayer.set('name',name);
			this.map.addLayer(newLayer);

			return this.getCurrentLayerByName(name);

		}

	}

	private interfaceCycle(toggle){

		for (let i in this.interfaceDisplay) {
			if(toggle !== i) this.interfaceDisplay[i] = false;
		}

	}

	mapToggles(evt) {

		for (let i in evt) {
			if(this.mapDisplay[i]) {
				this.mapDisplay[i] = evt[i];
			}
			let layer = this.getCurrentLayerByName(i);
			if(layer) {
				layer.setVisible(evt[i]);
			}

		}

	}

	interfaceToggle(toggle:string) {

		this.interfaceCycle(toggle);
		this.interfaceDisplay[toggle] ? this.interfaceDisplay[toggle] = false : this.interfaceDisplay[toggle] = true;		

	}

	interfaceToggles(evt){

		if(evt.toggle){
			
			this.interfaceToggle(evt.toggle);
		
		}

		if(evt.close && evt.close.source) {

			this.source = null;
			let IEP = this.appData.getItemEmitParameters();
			if(IEP.source) delete IEP.source;

			this.appData.setItemEmitParams(IEP);
			this.router.navigate(['/']);


		}

		if(evt.sourceType) {

			this.source = null;
			let IEP = this.appData.getItemEmitParameters();
			if(IEP.source) delete IEP.source;
			IEP.type = evt.sourceType;

			this.appData.setItemEmitParams(IEP);
			this.router.navigate(['/']);

		}

	}

}