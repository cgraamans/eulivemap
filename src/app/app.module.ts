import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { LocalStorageModule } from 'angular-2-local-storage';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

import { SocketService } from './services/socket.service';
import { DataService } from './services/data.service';
import { MapService } from './services/map.service';
import { AppDataService } from './services/app-data.service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

import { FileDropModule } from 'ngx-file-drop';
import { MarkdownModule } from 'ngx-markdown';

import { StopClickPropagationDirective } from './directives/stop-click-propagation.directive';

import { ModalRegisterComponent } from './components/modal-register/modal-register.component';
import { ModalUserComponent } from './components/modal-user/modal-user.component';
import { ModalItemComponent } from './components/modal-item/modal-item.component';

import { MapLeftNavComponent } from './components/map-left-nav/map-left-nav.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { TopLogoComponent } from './components/top-logo/top-logo.component';

import { OverlayLogComponent } from './components/overlay-log/overlay-log.component';

import { ItemComponent } from './components/item/item.component';
import { CommentComponent } from './components/comment/comment.component';

import { ProfileUserComponent } from './components/profile-user/profile-user.component';

import { ProfileOverlayUserComponent } from './components/profile-overlay-user/profile-overlay-user.component';
import { AppModalComponent } from './components/app-modal/app-modal.component';
import { CommentBoxComponent } from './components/comment-box/comment-box.component';
import { SourceEmitComponent } from './components/source-emit/source-emit.component';
import { CommentReportsComponent } from './components/comment-reports/comment-reports.component';
import { TopErrorsComponent } from './components/top-errors/top-errors.component';
import { ModalBetaComponent } from './components/modal-beta/modal-beta.component';


const config: SocketIoConfig = { url: '192.168.178.248:9001', options: {} };

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		ModalRegisterComponent,
		MapLeftNavComponent,
		TopNavComponent,
		TopLogoComponent,
		OverlayLogComponent,
		ItemComponent,
		StopClickPropagationDirective,
		CommentComponent,
		ModalUserComponent,
		ProfileUserComponent,
		ProfileOverlayUserComponent,
		ModalItemComponent,
		AppModalComponent,
		CommentBoxComponent,
		SourceEmitComponent,
		CommentReportsComponent,
		TopErrorsComponent,
		ModalBetaComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FileDropModule,
		MarkdownModule.forRoot(),
		SocketIoModule.forRoot(config),
	    LocalStorageModule.withConfig({
	        prefix: 'app',
	        storageType: 'localStorage'
	    }),
        ModalModule.forRoot(),
        FormsModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        TypeaheadModule.forRoot()
	],
	providers: [
		SocketService, 
		DataService,
		AppDataService,
		MapService,
	],
	bootstrap: [
		AppComponent
	],
	entryComponents:[
		ModalRegisterComponent,
		ModalUserComponent,
		ModalItemComponent,
		ModalBetaComponent
	]
})
export class AppModule { }
