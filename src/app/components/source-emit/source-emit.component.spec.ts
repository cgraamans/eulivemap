import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceEmitComponent } from './source-emit.component';

describe('SourceEmitComponent', () => {
  let component: SourceEmitComponent;
  let fixture: ComponentFixture<SourceEmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceEmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceEmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
