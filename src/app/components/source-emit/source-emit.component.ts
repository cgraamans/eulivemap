import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SocketService } from '../../services/socket.service';

@Component({
  selector: 'app-source-emit',
  templateUrl: './source-emit.component.html',
  styleUrls: ['./source-emit.component.scss']
})
export class SourceEmitComponent implements OnInit {

	private sourceObservable;

	constructor(private socket:SocketService,private route:ActivatedRoute) { }

	ngOnInit() {

		this.sourceObservable = this.route.params.subscribe(obs=>{
			if(obs && obs.source) this.socket.emit('source',{stub:obs.source})

		});

	}

	ngOnDestroy(){

		this.sourceObservable.unsubscribe();

	}

}
