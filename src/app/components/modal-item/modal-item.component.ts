import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute,Router,ParamMap,NavigationExtras } from '@angular/router';

import { AppDataService } from '../../services/app-data.service';
import { DataService } from '../../services/data.service';
import { SocketService } from '../../services/socket.service';

@Component({
  selector: 'app-modal-item',
  templateUrl: './modal-item.component.html',
  styleUrls: ['./modal-item.component.scss']
})
export class ModalItemComponent implements OnInit, OnDestroy {

	private subscriptions = [];
	public sections = {
		item:true,
		parent:true,
		comments:true
	};

	public item:App.Obj.ItemInterface = null;
	public comments:Array<App.Obj.CommentInterface> = [];
	public parent:App.Obj.CommentInterface|null = null;
	public commentParentStub:string|null = null;

	public statics;

	public isLoggedIn:boolean = false;
	public isMod:boolean = false;

	constructor(
		private socket:SocketService, 
		private appData:AppDataService,
		private data:DataService,
		private route:ActivatedRoute,
		private router:Router
	) {

		this.statics = this.data.getStaticObj();

	}

	ngOnInit() {

		this.clearNavObjects();

		this.subscriptions.push(this.data.getAuthObs().subscribe(obs=>{

			if(obs && this.isLoggedIn === false) {

				this.isLoggedIn = true;
				if(obs.auth && obs.auth > 0) this.isMod = true;

			} else {

				this.isLoggedIn = false;

			}

		}));

		this.subscriptions.push(this.route.params.subscribe(obs=>{

			if(obs && obs.item) {

				this.appData.setItem(null);
				this.socket.emit('item',{stub:obs.item});

			}

		}));

		this.subscriptions.push(this.appData.getItemObs().subscribe(obs=>{

			if(obs && obs.stub) {

				this.item = obs;
				let emitParams:App.Obj.CommentQueryParamsInterface = {
					item:obs.stub,
				};

				if (this.route.snapshot.queryParams && this.route.snapshot.queryParams.comment) emitParams.parent = this.route.snapshot.queryParams.comment;
				this.appData.setCommentEmitParams(emitParams);

			}

		}));

		this.subscriptions.push(this.appData.getCommentsObs().subscribe(obs=>{

			this.comments = obs;

		}));

		this.subscriptions.push(this.appData.getCommentEmitParametersObs().subscribe(obs=>{

			this.appData.setComments([]);
			this.appData.setCommentParent(null);

			if(this.item) {

				this.socket.emit('comments',obs);

				let emitParams = {item:this.item.stub,stub:obs.parent};
				if(obs.parent) this.socket.emit('comment.parent',emitParams);

			}

		}));

		this.subscriptions.push(this.appData.getCommentParentObs().subscribe(obs=>{

			this.parent = obs;
			this.commentParentStub = null;
			if(obs && obs.stub) this.commentParentStub = obs.stub;

		}));

		this.subscriptions.push(this.route.queryParams.subscribe(obs=>{

			let emitParams = this.appData.getCommentEmitParameters();
			if(emitParams.parent) delete emitParams.parent;

			if(emitParams.item) {

				if(obs.comment) emitParams.parent = obs.comment;

				this.appData.setCommentEmitParams(emitParams);

			}

		}));

	}

	ngOnDestroy(){

		this.subscriptions.forEach(subscription=>{
			subscription.unsubscribe();
		});
		this.clearNavObjects();


	}

	private clearNavObjects(){

		this.appData.setItem(null);
		this.appData.setComments([]);
		this.appData.setCommentParent(null);

	}

	private navToObj(obj) {

		this.router.navigate(
			[], obj
		);

	}

	itemToggles(event){

		if(event.like && this.isLoggedIn) this.socket.emit('user.set.item.like',{like:event.like.dir,stub:event.like.stub});
		if(event.location) {

			this.socket.emit('item.locations',{stub:event.location});
			this.router.navigate(['/']);

		}

	}

	gotoParentOfParentComment(){

		let navObj:NavigationExtras = {
			relativeTo: this.route,
			queryParams:{comment:this.parent.stub_parent}
		};
		this.navToObj(navObj);

	}

	setParentQueryParamComment(comment?:string){

		let parentQueryParams:{comment?:string} = {};
		let navObj:NavigationExtras = {
			relativeTo: this.route,
		};

		if(comment) {
			parentQueryParams.comment = comment;
		}
		navObj.queryParams = parentQueryParams;
		this.navToObj(navObj);

	}

	setSort(type){

		let emitParams = this.appData.getCommentEmitParameters();
		emitParams.sortBy = type;
		this.appData.setCommentEmitParams(emitParams);

	}

	commentToggles(event) {

		// LIKE
		if(event.like) this.socket.emit('user.set.comment.like',{like:event.like.dir,stub:event.like.stub});

		// SET PARENT
		if(event.comment) this.setParentQueryParamComment(event.comment);
		
		// USER ACTIONS
		if(this.isLoggedIn) {

			if(event.delete) this.socket.emit('user.set.comment.delete',event.delete);
			if(event.report) this.socket.emit('user.set.comment.report',event.report);

			// MODS
			if(this.isMod) {

				if(event.ban && this.isLoggedIn && this.data.getAuth().auth > 0) {

					this.socket.emit('mod.set.comment.ban',event.ban);
				}
				if(event.remove && this.isLoggedIn && this.data.getAuth().auth > 0) this.socket.emit('mod.set.comment.remove',event.remove);

			}

		}

	}

	displayToggles(type){

		if(typeof this.sections[type] === 'boolean') this.sections[type] = !this.sections[type];

	}

}
