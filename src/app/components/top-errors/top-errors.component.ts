import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../services/data.service';

import {Subscription} from 'rxjs'

export interface AppError extends App.Obj.ErrorInterface {
	elapsed:string;
};

@Component({
  selector: 'top-errors',
  templateUrl: './top-errors.component.html',
  styleUrls: ['./top-errors.component.scss']
})
export class TopErrorsComponent implements OnInit,OnDestroy {

	private subscriptions:Array<Subscription> = [];

	public error:AppError|null = null;

	constructor(private data:DataService) { }

	ngOnInit() {

		this.subscriptions.push(this.data.getErrorsObs().subscribe((obs:Array<AppError>)=>{

			this.error = null;
			if(obs.length>0) {
				this.error = obs[0];
				this.error.elapsed = this.getTimeDiff(obs[0].dt);
			}

		}));

	}

	ngOnDestroy() {

		this.subscriptions.forEach(subscription=>{
			subscription.unsubscribe();
		});

	}

	close() {
		
		this.data.removeError(this.error.id);

	}

	private getTimeDiff(dt) {

		let now = Math.floor((new Date()).getTime()/1000);
		let elapsed = now - dt;

		if(elapsed < 0){

			return '0 s';
		
		}

		if(elapsed < 60) {

			return elapsed+' s';

		}
		if(elapsed < 3600) {

			return Math.floor((elapsed/60)*10)/10+' m';

		}

		if(elapsed < 86400) {

			return Math.floor((elapsed/3600)*10)/10+' h';

		}

		if(elapsed >= 86400) {

			return Math.floor((elapsed/86400)*10)/10+' d';

		}

	}

}
