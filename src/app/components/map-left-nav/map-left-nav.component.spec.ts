import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapLeftNavComponent } from './map-left-nav.component';

describe('MapLeftNavComponent', () => {
  let component: MapLeftNavComponent;
  let fixture: ComponentFixture<MapLeftNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapLeftNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapLeftNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
