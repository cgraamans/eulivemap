import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'map-left-nav',
  templateUrl: './map-left-nav.component.html',
  styleUrls: ['./map-left-nav.component.scss']
})
export class MapLeftNavComponent implements OnInit {

	@Input() mapToggleList:{
		items:boolean;
		me:boolean;
		comments:boolean;
	} = {
		items:true,
		me:true,
		comments:true,
	};

	@Output() mapToggles: EventEmitter<any> = new EventEmitter();

	constructor() {}

	ngOnInit() {}

	mapToggle(type){

		let toggle = {};
		this.mapToggleList[type] = !this.mapToggleList[type];
		toggle[type] = this.mapToggleList[type]
		this.mapToggles.emit(toggle);

	}

}