import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayLogComponent } from './overlay-log.component';

describe('OverlayLogComponent', () => {
  let component: OverlayLogComponent;
  let fixture: ComponentFixture<OverlayLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
