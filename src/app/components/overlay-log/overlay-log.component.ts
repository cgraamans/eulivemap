import { Component, Input, Output, EventEmitter, HostListener, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { AppDataService } from '../../services/app-data.service';
import { SocketService } from '../../services/socket.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ModalRegisterComponent } from '../../components/modal-register/modal-register.component';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'overlay-log',
  templateUrl: './overlay-log.component.html',
  styleUrls: ['./overlay-log.component.scss']
})
export class OverlayLogComponent implements OnInit {

	@Input() source:App.Obj.SourceInterface|null = null;

  	@Input()
		set scrollTo(scrollTo: string|null) {
			if(scrollTo) {
				document.getElementById('#' + scrollTo).scrollIntoView({ behavior: 'smooth' });
				this._scrollTo = scrollTo;
	 		}
	 	}

	 	get scrollTo():string {
	 		return this._scrollTo;
	 	}

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();

	@HostListener('scroll', ['$event'])
		onElementScroll($event) {

			if($event && $event.srcElement && this.items.length>0) {

				if($event.srcElement.scrollTop + $event.srcElement.offsetHeight === $event.srcElement.scrollHeight) 
					this.itemUpTo.next(this.items[this.items.length-1].stub);

			}

		}

	public items:Array<App.Obj.ItemInterface> = [];

	public modalRef: BsModalRef;
	public statics = null;

	private itemUpTo:BehaviorSubject<string|null> = new BehaviorSubject(null);
	private _itemUpTo:string|null = null;
	private _scrollTo:string|null = null;

	private _isLoggedIn:BehaviorSubject<boolean> = new BehaviorSubject(false);
	public isLoggedIn:boolean = false;

	private subscriptions = [];

	constructor(private data:DataService, private socket:SocketService, private modalService:BsModalService, private appData:AppDataService) {

		this.statics = data.getStaticObj();

	}

	ngOnInit() {

		this.subscriptions.push(

			this.socket.on('source').subscribe((obs:{ok:boolean,results:Array<App.Obj.SourceInterface>})=>{

				if(obs.ok && obs.results.length > 0) {

					this.source = obs.results[0];

					let itemEmitParams:App.Obj.queryParamsInterface = this.appData.getItemEmitParameters();
					if(itemEmitParams) {
						itemEmitParams.source = this.source.stub;
						this.appData.setItemEmitParams(itemEmitParams);
					}

				}

			})

		);

		this.subscriptions.push(this.itemUpTo.subscribe(obs=>{

			if(obs && obs !== this._itemUpTo) {

				let fQP = this.appData.getItemEmitParameters();
				let nQP = Object.assign({upTo:obs},fQP);
				if(fQP && nQP) {
					this.socket.emit('items.upTo',nQP);
					this._itemUpTo = obs;
				}

			}

		}));

		this.subscriptions.push(this.appData.getItemsObs().subscribe(obs=>{
 
			this.items = obs;

		}));

		this.subscriptions.push(this.data.getAuthObs().subscribe(obs=>{
 
			if(obs) {

				if (this.isLoggedIn !== true) this.socket.emit('items',this.appData.getItemEmitParameters());
				this.isLoggedIn = true;

			} else {

				if (this.isLoggedIn !== false) this.socket.emit('items',this.appData.getItemEmitParameters());
				this.isLoggedIn = false;

			}

		}));

	}

	ngOnDestroy() {

		this.subscriptions.forEach(subscription=>{
			subscription.unsubscribe();
		});

	}

	itemToggles(event){

		if(event.like){

			if(!this.isLoggedIn) {

				this.modalRef = this.modalService.show(ModalRegisterComponent);

			} else {

				this.socket.emit('user.set.item.like',{like:event.like.dir,stub:event.like.stub});

			}

		}

		if(event.item){

			this.socket.emit('item',{stub:event.item});

		}

		if(event.source){

			this.socket.emit('source',{stub:event.source});

			let fqp = this.appData.getItemEmitParameters();
			if(fqp) fqp.source = event.source;
			this.appData.setItemEmitParams(fqp);

		}

		if(event.register) this.modalRef = this.modalService.show(ModalRegisterComponent);

		if(event.location) {

			this.socket.emit('item.locations',{stub:event.location});
			this.interfaceToggles.emit({toggle:'log'});

		}

	}

	setQP(type,val){

		let qP = this.appData.getItemEmitParameters();
		qP[type] = val;
		this.appData.setItemEmitParams(qP);

	}

	getQP(type){

		let qP = this.appData.getItemEmitParameters();
		return qP[type];

	}

	getTypeText(type?:string){
		if(type === '*' || type === 'A' || !type) return 'All';
		if(type === 'O') return 'Official';
		if(type === 'N') return 'News';
		if(type === 'T') return 'ThinkTanks';
		if(type === 'M') return 'Misc';		
	}

	getCurrentType(){
		let qP = this.appData.getItemEmitParameters();
		return this.getTypeText(qP.type);
	}

	getSourceType(type:string) {
		return this.getTypeText(type);
	}

	closeSourceBox(){

		this.source = null;
		this.interfaceToggles.emit({close:{'source':true}});

	}

	setType(type) {
		this.source = null;
		this.interfaceToggles.emit({'sourceType':type});		
	}

}
