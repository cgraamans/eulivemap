import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { DataService } from '../../services/data.service';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/';
import { ModalUserComponent } from '../../components/modal-user/modal-user.component';
import { ModalBetaComponent } from '../../components/modal-beta/modal-beta.component';

@Component({
	selector: 'top-nav',
	templateUrl: './top-nav.component.html',
	styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

	@Output() interfaceToggles: EventEmitter<any> = new EventEmitter();
	@Input() interfaceDisplay:{log?:boolean} = {};

	private subscriptions:Array<any> = [];
	public isLoggedIn:boolean = false;
	public modalRef: BsModalRef;

	constructor(private data:DataService,private modalService: BsModalService) { 

		this.subscriptions.push(data.getAuthObs().subscribe(obs=>{

			if(obs) {

				this.isLoggedIn = true;
			
			} else {
			
				this.isLoggedIn = false;
			
			}
		
		}));

	}

	ngOnInit() {
		
	}

	toggle(toggleElement){

		this.interfaceToggles.emit({toggle:toggleElement});

	}

	openBetaModal() {
			this.modalRef = this.modalService.show(ModalBetaComponent);
	
	}

	openUserModal() {
		this.modalRef = this.modalService.show(ModalUserComponent);
	}

	ngOnDestroy(){
		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}
		// for(let i=0,c=this.intervals.length;i<c;i++){
		// 	clearInterval(this.intervals[i]);
		// }
	}


}
