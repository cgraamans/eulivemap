import { Component, OnInit, TemplateRef } from '@angular/core';

import { DataService } from '../../services/data.service';

import { ModalRegisterComponent } from '../../components/modal-register/modal-register.component';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'top-logo',
  templateUrl: './top-logo.component.html',
  styleUrls: ['./top-logo.component.scss']
})
export class TopLogoComponent implements OnInit {

	private subscriptions:Array<any> = [];

	public userData:App.Obj.UserDataInterface|null = null;
	public modalRef: BsModalRef;
	public isLoggedIn:boolean = false;

	constructor(private data:DataService, private modalService: BsModalService) { 

		this.subscriptions.push(data.getUserDataObs().subscribe(obs=>{

			if(obs) this.userData = obs;

		}));

		this.subscriptions.push(data.getAuthObs().subscribe(obs=>{

			if(obs) {

				this.isLoggedIn = true;

			} else {

				this.isLoggedIn = false;
			
			}

		}));

	}

	ngOnInit() {
		
	}

	openModal() {

		if(!this.isLoggedIn) this.modalRef = this.modalService.show(ModalRegisterComponent);

	}

	ngOnDestroy(){

		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}
		// for(let i=0,c=this.intervals.length;i<c;i++){
		// 	clearInterval(this.intervals[i]);
		// }

	}

}
