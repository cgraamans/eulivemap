import { Component, OnInit, Input } from '@angular/core';
import { SocketService } from '../../services/socket.service';

@Component({
  selector: 'comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.scss']
})
export class CommentBoxComponent implements OnInit {

	@Input() parent:string = null;
	@Input() stub:string = null;
	@Input() text:string|null = '';
	@Input() item:string;

	private maxLength = 1000;
	public charCount = 1000;


	constructor(private socket:SocketService) { }

	ngOnInit() {}

	submit(){

		let emission:{parent?:string,stub?:string,text:string,item:string} = {text:this.text,item:this.item};

		if(this.parent) emission.parent = this.parent;
		if(this.stub) emission.stub = this.stub;

		if(emission.text.length > 0) {
			this.socket.emit('user.set.comment',emission);
			this.text = '';
		}

	}

	charCounter() {

		if(this.text) {
			if(this.text.length > this.maxLength) this.text = this.text.slice(0,this.maxLength);
			this.charCount = this.maxLength - this.text.length;
		}
	
	}

}
