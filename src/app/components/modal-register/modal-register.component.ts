import { Component, OnInit, OnDestroy, ViewChild, ElementRef, NgZone, AfterViewInit } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { BsModalRef } from 'ngx-bootstrap';
import {FormControl} from '@angular/forms';

import { Observable, fromEvent, Subscription } from 'rxjs';

// import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/share';
// import 'rxjs/observable/fromEvent';


@Component({
	selector: 'app-modal-register',
	templateUrl: './modal-register.component.html',
	styleUrls: ['./modal-register.component.scss']
})
export class ModalRegisterComponent implements OnInit, OnDestroy, AfterViewInit {

	public formObj:App.FormObjLoginRegister = {
		login:{name:null,password:null},
		reg:{name:null,password:null},
		persistent:true
	};
	public valid:App.FormObjValid = {name:false,password:false};

	private subscriptions:Array<Subscription> = [];

	@ViewChild('inputRegName') inputRegNameRef: ElementRef;
	@ViewChild('inputRegPwd') inputRegPwdRef: ElementRef;
	@ViewChild('inputRegNameSM') inputRegNameSMRef: ElementRef;
	@ViewChild('inputRegPwdSM') inputRegPwdSMRef: ElementRef;

	constructor(private socket:SocketService,public bsModalRef:BsModalRef,private ngzone: NgZone) {}

	ngAfterViewInit() {

		this.ngzone.runOutsideAngular( () => {

			this.subscriptions.push(
				fromEvent(this.inputRegNameRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('app.verify.name',{name:this.formObj.reg.name});
					})
			);
			this.subscriptions.push(
				fromEvent(this.inputRegPwdRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('app.verify.password',{password:this.formObj.reg.password});
					})
			);

			this.subscriptions.push(
				fromEvent(this.inputRegNameSMRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('app.verify.name',{name:this.formObj.reg.name});
					})
			);
			this.subscriptions.push(
				fromEvent(this.inputRegPwdSMRef.nativeElement, 'keyup')
					.debounceTime(300)
					.subscribe(()=>{
						this.socket.emit('app.verify.password',{password:this.formObj.reg.password});
					})
			);

		});

	}

	ngOnInit() {

		this.subscriptions.push(this.socket.on('app.verify.name').subscribe(val=>{

			val.ok ? this.valid.name = true : this.valid.name = false;

		}));

		this.subscriptions.push(this.socket.on('app.verify.password').subscribe(val=>{

			val.ok ? this.valid.password = true : this.valid.password = false;

		}));

		this.subscriptions.push(this.socket.on('auth').subscribe(val=>{

			if(val.ok === true) this.bsModalRef.hide();

		}));

	}

	submit(){

		let formObjStore;
		let formTarget = 'app.user.register';

		if(this.formObj.login.name && this.formObj.login.password) {
			formTarget = 'app.user.login'
			formObjStore = Object.assign({},this.formObj.login);
		} 
		
		if(this.formObj.reg.name && this.formObj.reg.password && this.valid.name && this.valid.password) formObjStore = Object.assign({},this.formObj.reg);
		if(this.formObj.login.name && this.formObj.login.password) formObjStore = Object.assign({},this.formObj.login);

		if(formObjStore) {

			if(this.formObj.persistent) {
				formObjStore.persistent = true;
			} else {
				formObjStore.persistent = false;
			}
			this.socket.emit(formTarget,formObjStore);

		}

	}

	isNotEmpty(){

		return (this.formObj.reg.password && this.formObj.reg.name && this.valid.name && this.valid.password) || (this.formObj.login.name && this.formObj.login.password) ? true : false; 
	
	}

	ngOnDestroy() {

		for(let i=0,c=this.subscriptions.length;i<c;i++){
			this.subscriptions[i].unsubscribe();
		}

	}

}
