import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MarkdownService } from 'ngx-markdown';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

	@Input() statics:{};

	@Input() isLoggedIn:boolean;
	@Input() isMod:boolean = false;
	@Input()
		set comment(comment: App.Obj.CommentInterface|null) {
			if(comment) {
				if(comment.text) this.text = this.markdown.compile(comment.text);
				if(comment.dt) this.timeDiff = this.getTimeDiff(comment.dt);
				this._comment = comment;
			}
		}

		get comment():App.Obj.CommentInterface|null {
			return this._comment;
		};

	@Output() commentToggles:EventEmitter<any> = new EventEmitter();

	private _comment:App.Obj.CommentInterface;
	
	public text:string = '';
	public showCommentBox:boolean = false;
	public showReportedComment:boolean = false;
	public timeDiff:string = '0s';

	constructor(private markdown:MarkdownService) { }

	ngOnInit() {

		console.log(this.comment);
	}

	onEvent(event) {

		event.stopPropagation();

	}

	getTimeDiff(dt){

		let now = Math.floor((new Date()).getTime()/1000);
		let elapsed = now - dt;

		if(elapsed < 0){

			return '0s';
		
		}

		if(elapsed < 60) {

			return elapsed+'s';

		}
		if(elapsed < 3600) {

			return Math.floor((elapsed/60)*10)/10+'m';

		}

		if(elapsed < 86400) {

			return Math.floor((elapsed/3600)*10)/10+'h';

		}

		if(elapsed >= 86400) {

			return Math.floor((elapsed/86400)*10)/10+'d';

		}

	}

	setParent() {

		if(this.comment) this.commentToggles.emit({comment:this.comment.stub});

	}

	setLike(stub,dir){

		this.commentToggles.emit({like:{dir:dir,stub:stub}});

	}

	setDelete(stub,dir){

		this.commentToggles.emit({delete:{dir:dir,stub:stub}});

	}


	setRemove(stub,dir){

		this.commentToggles.emit({remove:{dir:dir,stub:stub}});

	}

	setBan(userName,stub,dir?) {
		if(!dir) dir = 1;
		this.commentToggles.emit({
			ban:{
				name:userName,
				active:dir,
				stub:stub
			}});

	}

	reportEmitter(event) {

		this.commentToggles.emit(event);

	}

}