import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'modal-beta',
  templateUrl: './modal-beta.component.html',
  styleUrls: ['./modal-beta.component.scss']
})
export class ModalBetaComponent implements OnInit {

	constructor(public bsModal:BsModalRef) { }

	ngOnInit() {
	}

	close() {

	}

}
