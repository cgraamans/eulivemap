import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBetaComponent } from './modal-beta.component';

describe('ModalBetaComponent', () => {
  let component: ModalBetaComponent;
  let fixture: ComponentFixture<ModalBetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
