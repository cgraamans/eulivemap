import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'profile-overlay-user',
  templateUrl: './profile-overlay-user.component.html',
  styleUrls: ['./profile-overlay-user.component.scss']
})
export class ProfileOverlayUserComponent {

	@Input() user:App.Obj.UserDataInterface;

	constructor() {}

}