import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileOverlayUserComponent } from './profile-overlay-user.component';

describe('ProfileOverlayUserComponent', () => {
  let component: ProfileOverlayUserComponent;
  let fixture: ComponentFixture<ProfileOverlayUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileOverlayUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileOverlayUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
