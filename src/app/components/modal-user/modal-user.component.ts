import { Component, OnInit, OnDestroy, ViewChild, ElementRef, NgZone } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import {FormControl} from '@angular/forms';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

import { SocketService } from '../../services/socket.service';
import { DataService } from '../../services/data.service';
import { MarkdownService } from 'ngx-markdown';

@Component({
  selector: 'app-modal-user',
  templateUrl: './modal-user.component.html',
  styleUrls: ['./modal-user.component.scss']
})
export class ModalUserComponent implements OnInit {

	user:App.Obj.UserDataInterface;

	public pct = 0;	
	public countries:Array<{name:string,countrycode:string}> = [];
	
	public profile:string = '';
	public profileMd:string = '';
	public selectedCountry;
	public avatarLocation:string = '';


	public selectedCountryCode = 'BE';
	public profileCharCountLeft = 255;
	public statics = null;

	public password = {
		rep:'',
		old:'',
	};

	public loader:{
		country?:boolean,
		profileInput?:boolean,
	} = {};

	private file: BehaviorSubject<File|null> = new BehaviorSubject(null);
	private fileReader:FileReader = new FileReader();

	constructor(private socket:SocketService,private data:DataService, private markdown:MarkdownService, private modal:BsModalRef) {

		this.statics = this.data.getStaticObj();
		this.socket.emit('data.get.countries',{all:true,sortBy:'name',sortDir:'ASC'});

		this.avatarLocation = this.statics.apiRoot+this.statics.icons+'eu.stars.png';

		this.data.getUserDataObs().subscribe(obs=>{
			if(obs) {

				if(obs.profile) {
					this.profile = obs.profile;
					this.profileCharCountLeft = 255 - this.profile.length;
					this.profileMd = markdown.compile(obs.profile);
				}

				if(obs.avatar) this.avatarLocation = this.statics.apiRoot + this.statics.avatars+obs.avatar+'.png?ts='+ (new Date()).getTime();

				this.user = obs;

			}


		});

		this.file.subscribe(obs=>{

			if(obs) this.uploadFile();

		});

		this.socket.on('user.set.avatar.done').subscribe(obs=>{

			if(obs.ok) {

				this.file.next(null);
				this.socket.emit('user.get.data',{self:true});

			} else {

				if(obs.msg) {

					this.data.setError(obs.msg);

				} else {

					console.log('user.set.avatar.done',obs);

				}

			}

		});

		this.socket.on('user.set.avatar.slice').subscribe(obs=>{
			
			try {

				let file = this.file.getValue();
				if(file && this.fileReader) {
					let place = obs.currentSlice * 100000;
					let slice = file.slice(place, place + Math.min(100000, file.size - place));
					this.fileReader.readAsDataURL(slice); 
				}

			} catch(e) {

				console.error('user avatar slice retrieval error',e);
			
			}

		});

		this.socket.on('data.get.countries').subscribe(obs=>{

			this.loader.country = false;
			if(obs && obs.ok) this.countries = obs.results;

		});
		
		this.socket.on('user.set.profile').subscribe(obs=>{

			if(obs && obs.ok) {

				this.loader.profileInput = false;
				this.socket.emit('user.get.data',{});

			}

		});

	}

	ngOnInit() {

		this.socket.emit('user.get.data',{});

	}

	uploadFile() {

		try {

			let file = this.file.getValue();
			if(file) {
		
			    let slice = file.slice(0, 100000);

				this.fileReader.readAsDataURL(slice); 
				this.fileReader.onload = (evt) => {
					
					this.socket.emit('user.set.avatar', {
						fileData:{ 
							name: file.name, 
							type: file.type, 
							size: file.size, 
							data: this.fileReader.result
						}
					});
				
				};

			}

		} catch(e){

			this.data.setError('Upload Failed');
			console.error('UploadFile/user.avatar.set',e);
		
		}
	
	}

	setProfile() {

		if(this.profile	&& this.profile.length > 0) this.socket.emit('user.set.profile',{text:this.profile});
	
	}

	dropped(event: UploadEvent) {

		for (const droppedFile of event.files) {

			if (droppedFile.fileEntry.isFile) {
				const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
				fileEntry.file((file: File) => {
					this.file.next(file);
				});
			}
		}

	}

	selectFile(event) {

		if(event.target && event.target.files && event.target.files.length > 0) this.file.next(event.target.files[0]);

	}

	setCountry(){

		if(this.selectedCountryCode) this.socket.emit('user.set.location',{country:this.selectedCountryCode});

	}

	selectCountry(evt){

		if(evt && evt.item && evt.item.countrycode) {
			this.selectedCountryCode = evt.item.countrycode;
			this.socket.emit('user.set.location',{country:this.selectedCountryCode});
		}

	}

	profileCharCounter() {

		if(this.profile && this.profile.length > 0) {
			if(this.profile.length > 255) this.profile = this.profile.slice(0,254);
			this.profileCharCountLeft = 255 - this.profile.length;
		}
	
	}

	logout() {

		this.socket.logout();
		this.modal.hide();

	}

}

// https://stackoverflow.com/questions/40214772/file-upload-in-angular
// https://medium.com/@Mewsse/file-upload-with-socket-io-9d2d1229494