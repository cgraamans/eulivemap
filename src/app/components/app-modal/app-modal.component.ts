import { Component, OnInit,HostListener, HostBinding, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './app-modal.component.html',
  styleUrls: ['./app-modal.component.scss']
})
export class AppModalComponent implements OnInit {

	constructor(private router:Router,private eRef: ElementRef) { }

	public in:boolean = false;

	ngOnInit() {

		setTimeout(()=>{this.in = true},300);

	}

	close() {

		this.in = false;
		setTimeout(()=>{this.router.navigate(['/'])},300);
		

	}

	onEvent(event){

		event.stopPropagation();

	}

}
