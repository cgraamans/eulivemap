import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';
import { MarkdownService } from 'ngx-markdown';

@Component({
  selector: 'profile-user',
  templateUrl: './profile-user.component.html',
  styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {

	@Input()
		set user(user: App.Obj.UserDataInterface|null) {
			
			if(user) {

				this.avatarLocation = this.statics.apiRoot + this.statics.icons + 'eu.stars.png';

				this._user = user;
				if(this.user.profile) this.profile = this.markdownService.compile(user.profile);
				if(this.user.dt_register) this.dtAgo = this.timeAgo(this.user.dt_register);
				if(this.user.avatar) this.avatarLocation = this.statics.apiRoot + this.statics.avatars + this.user.avatar + ".png?ts=" + (new Date()).getTime();

			}

		}

		get user():App.Obj.UserDataInterface|null {
			return this._user;
		}


	private _user:App.Obj.UserDataInterface|null = null;
	
	public profile:string = '';	 	
	public dtAgo:string = '';
	public avatarLocation:string = '';

	public isLoggedIn:boolean = false;
	public statics = null;

	constructor(private data:DataService,private markdownService: MarkdownService) {

		data.getAuthObs().subscribe(obs=>{
			if(obs) {

				this.isLoggedIn = true;

			} else {

				this.isLoggedIn = false;

			}

		});

		this.statics = this.data.getStaticObj();

	}

	timeAgo(dt) {

		let perMinute = 60;
		let perHour = perMinute * 60;
		let perDay = perHour * 24;
		let perMonth = perDay * 30;
		let perYear = perDay * 365;

		let elapsed = Math.floor((new Date()).getTime()/1000) - dt;
		if (elapsed < perMinute) {
			return Math.round(elapsed/1000) + ' seconds ago';
		} else if (elapsed < perHour) {
			return Math.round(elapsed/perMinute) + ' minutes ago';
		} else if (elapsed < perDay ) {
			return Math.round(elapsed/perHour ) + ' hours ago';
		} else if (elapsed < perMonth) {
			return '~' + Math.round(elapsed/perDay) + ' days ago';
		} else if (elapsed < perYear) {
			return '~' + Math.round(elapsed/perMonth) + ' months ago';
		} else {
			return '~' + Math.round(elapsed/perYear ) + ' years ago';
		}

	}

	ngOnInit() {}

}
