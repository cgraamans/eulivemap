import { Component, OnInit, Input, Output, EventEmitter, OnChanges  } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit, OnChanges {

	@Output() itemToggles: EventEmitter<any> = new EventEmitter();

	@Input() item:App.Obj.ItemInterface;
	@Input() isLoggedIn:boolean;
	@Input() scrollTo:string;

	@Input() statics:{}|null = null;
	@Input() itemLinkLocation:string;

	public timeDiff = "0s";
	public titleToHtml:string;

	constructor(private location: Location) {}

	ngOnInit() {

		if(this.item && this.item.dt_created) {

			this.timeDiff = this.getTimeDiff(this.item.dt_created);
			this.titleToHtml = escape(this.item.text);
			this.item.text = unescape(this.item.text);

		}

	}

	ngOnChanges(ch){

		if(this.item && this.item.dt_created) {
			this.timeDiff = this.getTimeDiff(this.item.dt_created);
		}

	}

	setRegister(){
		this.itemToggles.emit({register:true});
	}

	setItem(stub){

		// this.location.go('/item/'+stub);
		this.itemToggles.emit({item:stub});

	}

	setSource(stub) {
		this.itemToggles.emit({source:stub});
	}

	setLike(stub,dir){
		this.itemToggles.emit({like:{dir:dir,stub:stub}});
	}

	setLocation(stub){
		this.itemToggles.emit({location:stub});
	}

	getDt(dt) {
		let date = new Date(+dt);
		return date;
	}

	getTimeDiff(dt){

		let now = Math.floor((new Date()).getTime()/1000);
		let elapsed = now - dt;

		if(elapsed < 0){

			return '0 s';
		
		}

		if(elapsed < 60) {

			return elapsed+' s';

		}
		if(elapsed < 3600) {

			return Math.floor((elapsed/60)*10)/10+' m';

		}

		if(elapsed < 86400) {

			return Math.floor((elapsed/3600)*10)/10+' h';

		}

		if(elapsed >= 86400) {

			return Math.floor((elapsed/86400)*10)/10+' d';

		}

	}

	copyMessage(val: string){
	
		let selBox = document.createElement('textarea');
		selBox.style.position = 'fixed';
		selBox.style.left = '0';
		selBox.style.top = '0';
		selBox.style.opacity = '0';
		selBox.value = val;
		document.body.appendChild(selBox);
		selBox.focus();
		selBox.select();
		document.execCommand('copy');
		document.body.removeChild(selBox);
	
	}


}
