import { Component, OnInit, Input, Output, EventEmitter, ElementRef, HostListener, ViewChild, AfterViewInit } from '@angular/core';

@Component({
	selector: 'comment-reports',
	templateUrl: './comment-reports.component.html',
	styleUrls: ['./comment-reports.component.scss']
})
export class CommentReportsComponent implements OnInit, AfterViewInit {

 	private _comment:App.Obj.CommentInterface = null;
	private parentNode: any;

  	@Input()
		set comment(comment: App.Obj.CommentInterface|null) {

			if(comment) {

				this._comment = comment;
				if(typeof comment.user_reported === 'number') {

					this.reports.forEach((report,rIdx)=>{

						this.reports[rIdx].active = 0;
						if(report.type === comment.user_reported) this.reports[rIdx].active = 1;

					});

				}

	 		}

	 	}

	 	get comment():App.Obj.CommentInterface {

	 		return this._comment;

	 	}

 	@Output() reportEmitter:EventEmitter<any> = new EventEmitter();

	ngAfterViewInit(){}

	public isOpen = false;
	public reports = [
		
		{
			text:'none',
			type:0,
			active:0
		},
		{
			text:'discriminatory',
			type:1,
			active:0
		},
		{
			text:'abusive/harmful',
			type:2,
			active:0
		},
		{
			text:'spam',
			type:3,
			active:0
		},
		{
			text:'misleading/not relevant',
			type:4,
			active:0
		}

	];

	/*

		- discrimination
		- abusive/threatening
		- spam
		- not relevant / misleading

	*/

	constructor(private elementRef:ElementRef) { }

	ngOnInit() { }

	public reportComment(stub,type) {
		this.reportEmitter.emit({report:{stub:stub,type:type}});
		this.isOpen = false;
	}

}
