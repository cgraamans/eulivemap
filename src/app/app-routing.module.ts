import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component'
import { ModalItemComponent } from './components/modal-item/modal-item.component';
import { SourceEmitComponent } from './components/source-emit/source-emit.component';

const routes: Routes = [

	{ path: '', component: HomeComponent, outlet:'modal' },

	{ path: 'item/:item', component:ModalItemComponent },
	{ path: 'source/:source', component: SourceEmitComponent },
	// { path: 'u/:u', component: HomeComponent, outlet:'main' },	

	{ path: '**', component: HomeComponent, outlet:'modal' },

];

@NgModule({
	exports: [
		RouterModule
	],
	imports: [ 
		RouterModule.forRoot(routes) 
	],

})

export class AppRoutingModule { }
