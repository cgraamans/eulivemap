import { Component, OnInit, HostListener } from '@angular/core';
import { MapService } from './services/map.service';

import Map from 'ol/Map';
import View from 'ol/View';
import Feature from 'ol/Feature';

import {defaults as defaultControls} from 'ol/control';
import {Vector as VectorLayer,Tile as TileLayer } from 'ol/layer';
import {Vector as VectorSource, TileJSON } from 'ol/source';
import {transform, fromLonLat, toLonLat, METERS_PER_UNIT as OlMPU} from 'ol/proj';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	public title = 'EULiveMap';
	public map:Map;

	constructor(private mapSvc:MapService) {}

	onResize(evt){

			this.map.updateSize();
	}

	ngOnInit(){

		let mapSource = new TileJSON({
			url: 'https://maps.tilehosting.com/styles/positron.json?key=vpLONrAxm4w86yb8XcCo',
			crossOrigin: 'anonymous'
		});

		let layer = new TileLayer({
			visible: true,
			preload: Infinity,
			source: mapSource,
			name:'mapLayer'
		});

		let view = new View({
			center: fromLonLat([6.661594, 50.433237]),
			zoom: 5,
			minZoom:3,
			maxZoom:16
		});

		this.mapSvc.setMap(new Map({
			target: 'map',
			layers: [layer],
			view: view,
			loadTilesWhileInteracting: true,
			controls:defaultControls({
				attribution:false,
				zoom:false
			})
		}));

		this.map = this.mapSvc.getMap();

	}

}
