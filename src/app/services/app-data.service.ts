import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {
	
	private items:BehaviorSubject<Array<App.Obj.ItemInterface>> = new BehaviorSubject([]);
	private itemEmitParams:BehaviorSubject<App.Obj.queryParamsInterface> = new BehaviorSubject({
		type:'*'
	});
	private item:BehaviorSubject<App.Obj.ItemInterface|null> = new BehaviorSubject(null);
	private itemLocations:BehaviorSubject<Array<App.Obj.ItemLocationObjectInterface>> = new BehaviorSubject([]);
	
	private comments:BehaviorSubject<Array<App.Obj.CommentInterface>> = new BehaviorSubject([]);
	private commentEmitParams:BehaviorSubject<App.Obj.CommentQueryParamsInterface> = new BehaviorSubject({
		item:null,
		sortBy:'hot'
	});
	private commentParent:BehaviorSubject<App.Obj.CommentInterface|null> = new BehaviorSubject(null);
	private commentLocations:BehaviorSubject<Array<App.Obj.CommentLocationObjectInterface>> = new BehaviorSubject([]);

	constructor(){}

	getItems() {

		return this.items.getValue();

	}

	getItemsObs() {

		return this.items.asObservable();

	}

	getItem() {

		return this.item.getValue();

	}

	getItemObs() {

		return this.item.asObservable();

	}


	getItemLocationsObs() {

		return this.itemLocations.asObservable();

	}

	getItemLocations() {

		return this.itemLocations.getValue();

	}

	getComments() {

		return this.comments.getValue();

	}

	getCommentsObs(){

		return this.comments.asObservable();

	}

	getCommentParent() {

		return this.commentParent.getValue();

	}

	getCommentParentObs(){

		return this.commentParent.asObservable();

	}

	getItemEmitParameters() {

		return this.itemEmitParams.getValue();

	}

	getItemEmitParametersObs() {

		return this.itemEmitParams.asObservable();

	}

	getCommentEmitParameters() {

		return this.commentEmitParams.getValue();

	}

	getCommentEmitParametersObs() {

		return this.commentEmitParams.asObservable();

	}

	getCommentLocations() {

		return this.commentLocations.getValue();

	}

	getCommentLocationsObs() {
		
		return this.commentLocations.asObservable();

	}

	setItems(items){

		this.items.next(items);

	}

	setItem(item){

		this.item.next(item);

	}

	setItemLocations(locationArr){

		this.itemLocations.next(locationArr);

	}

	setComments(comments?){

		this.comments.next(comments);

	}

	setCommentParent(comment:App.Obj.CommentInterface|null) {

		this.commentParent.next(comment)

	}

	setItemEmitParams(params:App.Obj.queryParamsInterface) {

		this.itemEmitParams.next(params);

	}

	setCommentEmitParams(params:App.Obj.CommentQueryParamsInterface){

		this.commentEmitParams.next(params);

	}

	setCommentLocations(locations) {

		this.commentLocations.next(locations);

	}

}
