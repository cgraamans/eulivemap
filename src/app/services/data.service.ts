import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class DataService {

	private StaticObj = {
		apiRoot:'https://api.euspider.eu/assets/',
		avatars:'avatars/',
		icons:'icons/',
		flags:'flags/',
		www:'https://euspider.eu/'
	};

	private auth:BehaviorSubject<App.Obj.AuthInterface|null> = new BehaviorSubject(null);
	private errors:BehaviorSubject<App.Obj.ErrorInterface[]> = new BehaviorSubject([]);
	
	private userData:BehaviorSubject<App.Obj.UserDataInterface|null> = new BehaviorSubject(null);
	// private userPosition:BehaviorSubject<App.Obj.UserLocationInterface|null> = new BehaviorSubject(null);

	constructor(private localStorageService: LocalStorageService) {

		// if (navigator && navigator.geolocation) {
		// 	navigator.geolocation.watchPosition(
		// 		position => {
		// 			this.userPosition.next(
		// 				{
		// 					latitude:position.coords.latitude,
		// 					longitude:position.coords.longitude,
		// 					accuracy:position.coords.accuracy,
		// 					altitude:position.coords.altitude,
		// 					altitudeAccuracy:position.coords.altitudeAccuracy,
		// 					heading:position.coords.heading,
		// 					speed:position.coords.speed,
		// 				}
		// 			);

		// 		},
		// 		error => {

		// 			this.userPosition.next(null);
		// 			console.log(error);

		// 			//     switch (error.code) {
		// 			//         case 1:
		// 			//             console.log('Permission Denied');
		// 			//             break;
		// 			//         case 2:
		// 			//             console.log('Position Unavailable');
		// 			//             break;
		// 			//         case 3:
		// 			//             console.log('Timeout');
		// 			//             break;
		// 			//     }
		
		// 		}
		
		// 	);
		
		// }

	 }

	getAuth() {

		return this.auth.getValue();

	}

	getAuthObs() {

		return this.auth.asObservable();

	}

	getStaticObj(){
	
		return Object.assign({},this.StaticObj);
	
	}


	getStore(keyName:string){

		return this.localStorageService.get(keyName);

	}

	getUserDataObs() {

		return this.userData.asObservable();

	}

	getUserData(){

		return this.userData.getValue();

	}

	getErrors(){

		return this.errors.getValue();

	}

	getErrorsObs() {

		return this.errors.asObservable();

	}

	setStore(keyName:string,data:{}){

		return this.localStorageService.set(keyName,data);

	}

	setUserData(userData) {

		this.userData.next(userData);

	}

	setErrors(errors?:Array<App.Obj.ErrorInterface>|null) {

		if(!errors) errors = [];
		this.errors.next(errors);

	}

	setError(errorMessage,type?:string) {

		let Errors = this.errors.getValue();
		let errorItem:App.Obj.ErrorInterface = {
			dt:Math.round((new Date()).getTime()/1000),
			msg:errorMessage,
			id:(Error.length)
		}
		if(!type) type = 'E';
		if(type && type.length === 1) errorItem.type = type.toUpperCase();
		Errors.unshift(errorItem);
		this.errors.next(Errors);

	}

	removeError(id){

		let Errors = this.errors.getValue();
		if(Errors && Errors.length > 0) {

			let eIdx = Errors.findIndex(x=>x.id === id);
			if(eIdx > -1) {
				Errors.splice(eIdx,1);
			}
			this.errors.next(Errors);

		}

	}

	clearErrors(){

		this.errors.next([]);

	}

	clearAuth() {

		this.userData.next(null);
		this.auth.next(null);
		this.localStorageService.clearAll();

	}

	setAuth(d?:App.Obj.AuthInterface){

		if(!d){

			this.clearAuth();

		} else {

			if(d.apiId && d.token || d.name && d.auth > -1) {

				let dataObj = {
					auth:d.auth,
					apiId:d.apiId,
					token:d.token,
					name:d.name
				};
				this.auth.next(dataObj);
				this.setStore('_user',dataObj);

			}

		}

	}

}
