import { Injectable } from '@angular/core';

import Map from 'ol/Map';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class MapService {

	private map:Map;

	constructor() {

		try {


			// console.log(this.obj);

			// this.obj.on('click', function(evt) {

			// 	// console.log(toLonLat(evt.coordinate));
			// 	// console.log(that.map.getView().getZoom());

			// 	let iCount = 0;
			// 	var feature = this.obj.forEachFeatureAtPixel(evt.pixel,function(feature, layer) {

			// 		console.log('feature under click:',feature);

			// 		let item = feature.get('item');
			// 		if(iCount <1 && item) { 

			// 			that.scrollTo = item.stub;
						
			// 			let coords = transform([item.source_lon, item.source_lat], 'EPSG:4326', 'EPSG:3857');
			// 			if(item.lon && item.lat) coords = transform([item.lon, item.lat], 'EPSG:4326', 'EPSG:3857');

			// 			let view = that.map.getView();
			// 			view.animate({
			// 				center:coords,
			// 				duration:2000
			// 			});

			// 			that.interfaceToggle('log');

			// 		}					

			// 		let me = feature.get('me');
			// 		if(me) {
			// 			console.log('me',me);
			// 		}

			// 		iCount++;

			// 	});

			// 	console.log('done');
			// });

			// this.map.getCurrentScale = function () {

			// 	let view = that.map.getView();
			// 	if(view) {
			// 		let resolution = view.getResolution();
			// 		let units = view.getProjection().getUnits();		
			// 		let mpu = OlMPU[units];
			// 		let dpi = 25.4 / 0.28;

			// 		if(resolution && units && mpu) return (resolution * mpu * 39.37 * dpi);

			// 	}
			// 	return false;

			// };

		} catch(e) {

			console.log(e);

		}

	}

	setMap(map:Map){

		this.map = map;

	}

	getMap(){
		return this.map;
	}

}
