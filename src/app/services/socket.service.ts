import { Injectable } from '@angular/core';

import { Socket } from 'ngx-socket-io';
import * as jwt from 'jsonwebtoken';

import { DataService } from './data.service';
import { AppDataService } from './app-data.service';

import { BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromEvent';

@Injectable()
export class SocketService {

	private token:BehaviorSubject<string> = new BehaviorSubject(null);

	constructor(private socket:Socket, private data:DataService, private appData:AppDataService) {

		// INIT
		this.on('init',true).subscribe(init=>{

			if(init.ok===true) {

				this.token.next(init.secret);

				// GET LOGIN STORE
				let val:any = this.data.getStore('_user');
				if(val){

					if((val.apiId) && (val.token)) {

						this.emit('app.user.login',{apiId:val.apiId,token:val.token});

					}

				}

			}

		});

		// AUTH
		this.on('auth').subscribe(obs=>{

			if(obs && obs.ok === true) {
				if(obs.user) {
				
					this.data.setAuth(obs.user);
					this.emit('user.get.data',{});

				} else {
				
					this.error('no user in auth message');
				
				}
			} else {

				this.data.setAuth(null);

				if(obs.msg) this.error(obs.msg);
				if(obs.e || obs.err) console.log(obs);
			}

		});

		// 
		this.on('app.user.logout').subscribe(obs=>{

			if(obs.ok) this.data.clearAuth();

		});

		// ITEMS
		this.on('items').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					if(obs.results.length > 0) {

						this.appData.setItems(obs.results);
						let resList = [];
						for(let i = 0, c = obs.results.length > 20 ? 20 : obs.results.length; i < c; i++){
							resList.push(obs.results[i].stub);
						}

						this.emit('item.locations',{stubs:resList});

					}

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		// items upto
		this.on('items.upTo').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					if(obs.results.length > 0) {

						let items = this.appData.getItems();
						if(items.length > 0) {

							let newItems = items.concat(obs.results);
							this.appData.setItems(newItems);

						}
					}

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		//items from stub
		this.on('items.from').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					if(obs.results.length > 0) {

						let items = this.appData.getItems();
						if(items.length > 0) {

							let newItems = obs.results.concat(items);
							this.appData.setItems(newItems);

						}

					}

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		// user data
		this.on('user.get.data').subscribe(obs=>{

			if(obs && obs.ok && obs.result) {

				this.data.setUserData(obs.result);

			} else {

				if(obs.msg) this.error(obs.msg);
				if(obs.e || obs.err) console.log(obs);

			}

		});


		// item
		this.on('item').subscribe(obs=>{

			if(obs.ok && obs.results.length > 0) {

				this.appData.setItem(obs.results[0]);

			} else {

				if(obs.msg) this.error(obs.msg);
				if(obs.e || obs.err) console.log(obs);

			}

		});


		// ITEM LIKES
		this.on('user.set.item.like').subscribe(itemRes=>{

			if(itemRes.ok) {
				
				let getValArr = this.appData.getItems();
				let itemId = getValArr.findIndex(x=>x.stub === itemRes.stub);
				if(itemId > -1) {
				
					let item = getValArr[itemId];
					item.user_liked = itemRes.like;
					if(itemRes.like < 1) {
						item.data_likes--;
					} else {
						item.data_likes++;
					}
					this.appData.setItems(getValArr);
				
				}

				let getValOfItem = this.appData.getItem();
				if(getValOfItem && getValOfItem.stub && getValOfItem.stub === itemRes.stub) {

					getValOfItem.user_liked = itemRes.like;
					if(itemRes.like < 1) {
						getValOfItem.data_likes--;
					} else {
						getValOfItem.data_likes++;
					}
					
				}

			} 

		});

		// COMMENT LIKES
		this.on('user.set.comment.like').subscribe(obs=>{

			if(obs.ok && typeof obs.like === 'number' && obs.like > -1) {
				
				let getValArr = this.appData.getComments();
				let commentId = getValArr.findIndex(x=>x.stub === obs.stub);
				if(commentId > -1) {				

					getValArr[commentId].user_liked = obs.like;
					if(obs.like < 1) {
						getValArr[commentId].data_likes--;
					} else {
						getValArr[commentId].data_likes++;
					}
					this.appData.setComments(getValArr);
				
				}

				let getValOfParent = this.appData.getCommentParent();
				if(getValOfParent && getValOfParent.stub && getValOfParent.stub === obs.stub) {

					getValOfParent.user_liked = obs.like;
					if(obs.like < 1) {
						getValOfParent.data_likes--;
					} else {
						getValOfParent.data_likes++;
					}
					
				}

			} 

		});

		this.on('user.set.comment.delete').subscribe(obs=>{

			if(obs.ok && obs.result && obs.result.stub) {
				
				let getValArr = this.appData.getComments();
				let commentId = getValArr.findIndex(x=>x.stub === obs.result.stub);
				if(commentId > -1) {				

					getValArr[commentId]= obs.result;
					this.appData.setComments(getValArr);

				}

				let getValOfParent = this.appData.getCommentParent();
				if(getValOfParent && getValOfParent.stub && getValOfParent.stub === obs.result.stub) this.appData.setCommentParent(obs.result);

			} 

		});


		this.on('user.set.comment.report').subscribe(obs=>{

			if(obs.ok && obs.result && obs.result.stub) {
				
				let getValArr = this.appData.getComments();
				let commentId = getValArr.findIndex(x=>x.stub === obs.result.stub);
				if(commentId > -1) {				

					getValArr[commentId]= obs.result;
					this.appData.setComments(getValArr);

				}

				let getValOfParent = this.appData.getCommentParent();
				if(getValOfParent && getValOfParent.stub && getValOfParent.stub === obs.result.stub) this.appData.setCommentParent(obs.result);

			} 

		});


		this.on('mod.set.comment.remove').subscribe(obs=>{

			if(obs.ok && obs.result && obs.result.stub) {
				
				let getValArr = this.appData.getComments();
				let commentId = getValArr.findIndex(x=>x.stub === obs.result.stub);
				if(commentId > -1) {				

					getValArr[commentId]= obs.result;
					this.appData.setComments(getValArr);

				}

				let getValOfParent = this.appData.getCommentParent();
				if(getValOfParent && getValOfParent.stub && getValOfParent.stub === obs.result.stub) this.appData.setCommentParent(obs.result);

			} 

		});

		this.on('mod.set.comment.ban').subscribe(obs=>{

			if(obs.msg) this.error(obs.msg);

		});
		// locations
		this.on('item.locations').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					this.appData.setItemLocations(obs.results);

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}


		});

		// locations
		this.on('comment.locations').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					this.appData.setCommentLocations(obs.results);

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		// comments
		this.on('comments').subscribe(obs=>{

			if(obs) {

				if(obs.ok && obs.ok === true && obs.results) {

					this.appData.setComments(obs.results);	

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		this.on('user.set.comment').subscribe(obs=>{
			
			if(obs) {

				if(obs.ok && obs.res) {

					if(obs.res.stub_parent) {
					
						this.socket.emit('comments',{parent:obs.res.stub_parent});
					
					}

					let comments = this.appData.getComments();
					if(comments.length > 0) {

						let cIdx = comments.findIndex(x=>x.stub === obs.res.stub);
						if(cIdx > -1) {
							comments[cIdx] = obs.res;
						} else {
							comments.unshift(obs.res);
						}

					} else {

						comments.unshift(obs.res);
					
					}
					this.appData.setComments(comments);

				} else {

					if(obs.msg) this.error(obs.msg);
					if(obs.e || obs.err) console.log(obs);

				}

			}

		});

		this.on('comment.parent').subscribe(obs=>{

			if(obs) {

				if(obs.ok) {

					if(obs.results && obs.results.length === 1) this.appData.setCommentParent(obs.results[0]);

				} else {

					if(obs.msg) {

						this.error(obs.msg);

					} else {

						console.log(obs);

					}

				}

			} 

		});

		// emit parameters
		this.appData.getItemEmitParametersObs().subscribe(params=>{

			this.emit('items',params);

		});

	 }

	emit(toSocket,message,noDecode=false){

		try {

			if(!noDecode) {

				let usr = this.data.getAuth();
				if(	(usr) && (typeof message === 'object')) message = Object.assign({user:usr},message);      
				
				this.encode(message,val=>{

					this.socket.emit(toSocket,val);

				});

			} else {

				this.socket.emit(toSocket,message);

			}

		} catch(e) {

			this.error('emit error');
			console.log(e);

		}

	}

	on(fromSocket,noDecode?) {

		let fE = this.socket
			.fromEvent(fromSocket);

		if(noDecode) return fE;
		return fE.map(msg=>this.decode(msg));

	}

	private encode(message,callback) {

		try {

			let val = this.token.getValue();
			if(!val){

				setTimeout(()=>{

					this.encode(message,callback);

				},1000);

			} else {

				callback(jwt.sign(message,val));

			}

		} catch(e) {

			this.error('encoding error');
			console.log(e);

		}

	}

	private decode(message) {

		let token = this.token.getValue();
		if(token){

			try {

				return jwt.verify(message,token);
		
			} catch(e) {
			
				this.error('jwt decoding error');
				console.log(e);
		
			}      
		
		} else {
		
			this.error('no valid token');
		
		}

	}

	error(e) {

		this.data.setError(e);

	}

	logout() {

		this.emit('app.user.logout',{});

	}

}
